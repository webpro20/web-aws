import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";
// import PointOfSell from "../views/PointOfSell.vue";
import LoginView from "@/views/LoginView.vue";
import MyDrawer from "@/components/menus/MyDrawer.vue";
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "login",
      component: LoginView,
    },
    // {
    //   path: "/login",
    //   name: "login",
    //   component: LoginView,
    // },
    {
      path: "/user",
      name: "user",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      // component: () => import("../views/user/UserView.vue"),
      components: {
        default: import("../views/user/UserView.vue"),
        menu: () => MyDrawer,
      },
      meta: {
        layout: "MainLayout",
        // layout: "FullLayout",
        // requiresAuth: true,
      },
    },
    {
      path: "/table",
      name: "Table",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      // component: () => import("../views/TableView.vue"),
      components: {
        default: import("../views/TableView.vue"),
        menu: () => MyDrawer,
      },
      meta: {
        layout: "MainLayout",
        // layout: "FullLayout",
        // requiresAuth: true,
      },
    },
    {
      path: "/tableCus",
      name: "TableCus",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      // component: () => import("../views/TableView.vue"),
      components: {
        default: import("../views/Customer/TableCusView.vue"),
        menu: () => MyDrawer,
      },
      meta: {
        layout: "FullLayout",
        // layout: "FullLayout",
        // requiresAuth: true,
      },
    },
    {
      path: "/cook",
      name: "Cook",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      // component: () => import("../views/cook/CookView.vue"),
      components: {
        default: import("../views/cook/CookView.vue"),
        menu: () => MyDrawer,
      },
      meta: {
        layout: "MainLayout",
        // layout: "FullLayout",
        // requiresAuth: true,
      },
    },
    {
      path: "/cookserve",
      name: "Cookserve",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      // component: () => import("../views/cook/CookserveView.vue"),
      components: {
        default: import("../views/cook/CookserveView.vue"),
        menu: () => MyDrawer,
      },
      meta: {
        layout: "MainLayout",
        // layout: "FullLayout",
        // requiresAuth: true,
      },
    },
    {
      path: "/users",
      name: "users",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: import("../views/UsersView.vue"),
        menu: () => MyDrawer,
      },
      meta: {
        layout: "MainLayout",
        // layout: "FullLayout",
        // requiresAuth: true,
      },
    },
    {
      path: "/point-of-sell",
      name: "Point of Sell",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      // component: () => import("../views/PointOfSell.vue"),
      components: {
        default: import("../views/PointOfSell.vue"),
        menu: () => MyDrawer,
      },
      meta: {
        // layout: "MainLayout",
        layout: "FullLayout",
        // requiresAuth: true,
      },
    },
    {
      path: "/theserve",
      name: "theserve",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      // component: () => import("../views/SearchView.vue"),
      components: {
        default: import("../views/ServeView.vue"),
        menu: () => MyDrawer,
      },
      meta: {
        layout: "MainLayout",
        // layout: "FullLayout",
        // requiresAuth: true,
      },
    },
    {
      path: "/foodStatus",
      name: "FoodStatus",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      // component: () => import("../views/FoodStatusView.vue"),
      components: {
        default: import("../views/FoodStatusView.vue"),
        menu: () => MyDrawer,
      },
      meta: {
        layout: "FullLayout",
        // layout: "FullLayout",
        // requiresAuth: true,
      },
    },

    {
      path: "/checkinout",
      name: "Checkinout",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      // component: () => import("../views/CheckInOutView.vue"),
      components: {
        default: import("../views/CheckInOutView.vue"),
        menu: () => MyDrawer,
      },
      meta: {
        layout: "MainLayout",
        // layout: "FullLayout",
        // requiresAuth: true,
      },
    },
    {
      path: "/salary",
      name: "Salary",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      // component: () => import("../views/SalaryView.vue"),
      components: {
        default: import("../views/SalaryView.vue"),
        menu: () => MyDrawer,
      },
      meta: {
        layout: "MainLayout",
        // layout: "FullLayout",
        // requiresAuth: true,
      },
    },
    {
      path: "/stock",
      name: "Stock",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      // component: () => import("../views/StockView.vue"),
      components: {
        default: import("../views/StockView.vue"),
        menu: () => MyDrawer,
      },
      meta: {
        layout: "MainLayout",
        // layout: "FullLayout",
        // requiresAuth: true,
      },
    },
    {
      path: "/bill",
      name: "Bill",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      // component: () => import("../views/BillView.vue"),
      components: {
        default: import("../views/BillView.vue"),
        menu: () => MyDrawer,
      },
      meta: {
        layout: "MainLayout",
        // layout: "FullLayout",
        // requiresAuth: true,
      },
    },
    {
      path: "/employee",
      name: "Employee",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      // component: () => import("../views/BillView.vue"),
      components: {
        default: import("../views/EmployeeView.vue"),
        menu: () => MyDrawer,
      },
      meta: {
        layout: "MainLayout",
        // layout: "FullLayout",
        // requiresAuth: true,
      },
    },
  ],
});

export default router;
