export default interface Stock {
  id: number;
  matCode: string;
  name: string;
  balance: number;
  unit: string;
  category: string;
  createdAt?: Date;
  updateAt?: Date;
  deleteAt?: Date;
}
