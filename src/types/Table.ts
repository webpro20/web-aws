export default interface Table {
  id?: number;
  name?: string;
  amount?: number;
  status?: string;
}
