import type OrderItem from "@/types/OrderItem";
export default interface Products {
  id?: number;
  orderItem?: OrderItem[];
  name?: string;
  // total?: number;
  price?: number;
  img?: string;
  category?: string;
}
