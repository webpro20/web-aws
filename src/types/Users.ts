export default interface Users {
  id: number;
  name: string;
  tel: string;
  type: string;
  time_inout: string;
  date: string;
}
