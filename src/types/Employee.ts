export default interface Employee {
  id: number;
  firstname: string;
  lastname: string;
  tel: string;
  email: string;
  position: string;
  hour: number;
  time_inout: string;
  date: string;
  createdAt?: Date;
  updateAt?: Date;
  deleteAt?: Date;
}
