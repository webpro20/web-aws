import type OrderItem from "@/types/OrderItem";
export default interface ProductQueue {
  id?: number;
  name?: string;
  price?: number;
  note?: string;
  status?: string;
  level?: string;
  orderItem?: OrderItem;
}
