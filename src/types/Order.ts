import type OrderItem from "./OrderItem";
import type OrderDetail from "./OrderItem";
import type Table from "./Table";

export default interface Order {
  id_order?: number;
  total?: number;
  amount?: number;
  status?: string;
  orderItems?: OrderItem[];
  // table_id: number;
  table?: Table;
}
