import http from "./axios";
import type Table from "@/types/Table";
function getTables() {
  return http.get("/table");
}

function saveTable(table: Table) {
  return http.post("/table", table);
}

function updateTable(id: number, table: Table) {
  return http.patch(`/table/${id}`, table);
}

function deleteTable(id: number) {
  return http.delete(`/table/${id}`);
}

export default {
  getTables,
  saveTable,
  updateTable,
  deleteTable,
};
