import type Product from "@/types/Products";
import http from "./axios";
function getProducts() {
  return http.get("/products");
}

// function saveProduct(product: Product) {
//   return http.post("/products", product);
// }

function updateProduct(id: number, product: Product) {
  return http.patch(`/products/${id}`, product);
}

function deleteProduct(id: number) {
  return http.delete(`/products/${id}`);
}
function saveProduct(product: Product & { files: File[] }) {
  const formData = new FormData();
  formData.append("name", product.name!);
  formData.append("price", `${product.price}`);
  // formData.append("total", `${product.total}`);
  formData.append("category", product.category!);
  formData.append("file", product.files[0]);
  return http.post("/products", formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
}
function getProductImg(id: number) {
  return http.get(`/products/${id}/image`);
}

export default {
  getProducts,
  getProductImg,
  saveProduct,
  updateProduct,
  deleteProduct,
};
