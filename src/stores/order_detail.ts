import { ref } from "vue";
import { defineStore } from "pinia";
import type Products from "@/types/Products";
import { useOrderStore } from "./order";
import type OrderItem from "@/types/OrderItem";
import type Order from "@/types/Order";
import { useTableStore } from "@/stores/table";
import orderService from "@/services/order";

export const useOrderDetailStore = defineStore("OrderDetail", () => {
  const order_item = ref<OrderItem[]>([]);
  const orderItems = ref<OrderItem[]>([]);

  const getAll = () => {
    return order_item;
  };
  const addItem = (
    product: Products,
    options: {
      addition: string;
      choice: number;
      choice_value: number;
      choice_label: string;
    }
  ) => {
    order_item.value.push({
      product: product,
      addition: options.addition,
      choice: options.choice,
      choice_value: options.choice_value,
      choice_label: options.choice_label,
      order_id: useOrderStore().getCurrentIndex(),
    });
    console.log("product: " + product);
    console.log("addition" + options.addition);
    console.log("choice" + options.choice);
    console.log("choice_value" + options.choice_value);
    console.log("choice_Label" + options.choice_label);
  };

  const removeItem = (index: number) => {
    console.log(index);
    // const _index = orderItems.value.findIndex(e => e.id === index);
    orderItems.value.splice(index, 1);
  };

  const save = async (order: Order) => {
    // useOrderStore().save(order_item.value);
    const newOrderItem = orderItems.value;
    const currentOrderId = useTableStore().currentOrderId;
    // const currentOrderID: number = currentOrder!.id_order;
    order_item.value = [];
    // order.orderItems = orderItems.value;
    // console.log("xxxxxxxxxxxxxxxxxx");
    // console.log(order.orderItems);
    const updateOrder: Order = {
      orderItems: newOrderItem,
    };
    orderService.updateOrder(currentOrderId!, updateOrder);

    console.log(currentOrderId);
  };

  const increaseItem = (product_id: number) => {
    console.log(product_id);
    const index = orderItems.value.findIndex(
      (e) => e.product?.id === product_id
    );
    orderItems.value[index].amount += 1;
  };
  const decreaseItem = (product_id: number) => {
    const index = orderItems.value.findIndex(
      (e) => e.product?.id === product_id
    );
    orderItems.value[index].amount--;
  };

  const findListIndexByProductID = (product_id: number) => {
    const index = orderItems.value.findIndex(
      (e) => e.product?.id === product_id
    );
    return index;
  };

  return {
    order_item,
    getAll,
    addItem,
    removeItem,
    increaseItem,
    decreaseItem,
    save,
    findListIndexByProductID,
    orderItems,
  };
});
