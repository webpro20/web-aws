import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type User from "@/types/User";
import userService from "@/services/user";
// import { useLoadingStore } from "./loading";
// import { useMessageStore } from "./message";

export const useUsersStore = defineStore("users", () => {
  const dialog = ref(false);

  const editedUsers = ref<User>({
    id: 0,
    email: "",
    name: "",
    password: "",
  });

  const lastId = 4;
  const users = ref<User[]>([
    // {
    //   id: 1,
    //   name: "JM",
    //   tel: "0999999999",
    //   type: "พนักงาน",
    //   time_inout: "08:00 - 17:00",
    //   date: "07/01/2566",
    // },
    // {
    //   id: 2,
    //   name: "RM",
    //   tel: "0963552664",
    //   type: "พนักงาน",
    //   time_inout: "13:00 - 21:00",
    //   date: "07/01/2566",
    // },
  ]);

  const getUsers = async () => {
    try {
      const res = await userService.getUsers();
      users.value = res.data;
      console.log(res.data);
    } catch (e) {
      console.log(e);
    }
    return;
  };

  async function saveUsers() {
    try {
      const res = await userService.saveUser(editedUsers.value);
      dialog.value = false;
      await getUsers();
    } catch (e) {
      console.log(e);
    }
  }

  async function deleteUsers(id: number) {
    try {
      const res = await userService.deleteUser(id);
      await getUsers();
    } catch (e) {
      console.log(e);
    }
  }

  const editUsers = (user: User) => {
    editedUsers.value = JSON.parse(JSON.stringify(user));
    dialog.value = true;
  };

  const clear = () => {
    editedUsers.value = {
      id: 0,
      email: "",
      name: "",
      password: "",
    };
  };

  return {
    users,
    deleteUsers,
    dialog,
    editedUsers,
    clear,
    saveUsers,
    editUsers,
    getUsers,
    // isTable,
  };
});
