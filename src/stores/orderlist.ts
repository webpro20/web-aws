import { ref } from "vue";
import { defineStore } from "pinia";
import type OrderList from "@/types/OrderList";

export const useCookStore = defineStore("cooking", () => {
  const cooking = ref<OrderList[]>([]); //กำลังเตรียมทำ
  const finish = ref<OrderList[]>([]); //เสร็จล้ะ
  const cookorder = ref<OrderList[]>([
    //รอจ้า
    {
      id: 1,
      num: 1,
      name: "พระจันทร์มันไก่",
      note: "ไม่ขม",
      level: "พิเศษ",
      table: 1,
      status: "รอ",
    },
    {
      id: 2,
      num: 1,
      name: "คะน้าหมูกรอบ",
      note: "ไม่ใส่คะน้า แจฮยอนชอบมาก",
      level: "พิเศษมากๆ",
      table: 1,
      status: "รอ",
    },
    {
      id: 3,
      num: 1,
      name: "ข้าวปลาทู",
      note: "แกะก้างออกด้วย เด๋วติดคอกัสเบลตุย",
      level: "พิเศษ",
      table: 2,
      status: "รอ",
    },
    {
      id: 4,
      num: 1,
      name: "ก๋วยเตี๋ยวกัญชา",
      note: "ไม่เอากันยา",
      level: "ธรรมดา",
      table: 3,
      status: "รอ",
    },
  ]);

  const cooklist = (id: number): void => {
    //รอ
    const index = cookorder.value.findIndex((item) => item.id === id);
    cooking.value.push(cookorder.value[index]);
    cookorder.value.splice(index, 1);
  };

  const finishlist = (id: number): void => {
    //ทำเสร็จ
    const index = cooking.value.findIndex((item) => item.id === id);
    finish.value.push(cooking.value[index]);
    cooking.value.splice(index, 1);
  };

  const deletecook = (id: number): void => {
    //ลบออเดอร์
    const index = cookorder.value.findIndex((item) => item.id === id);
    cookorder.value.splice(index, 1);
  };
  const deletefinish = (id: number): void => {
    //ลบที่ทำ
    const index = cooking.value.findIndex((item) => item.id === id);
    cooking.value.splice(index, 1);
  };

  return {
    cooking,
    cookorder,
    finish,
    cooklist,
    finishlist,
    deletecook,
    deletefinish,
  };
});
