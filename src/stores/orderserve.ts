import { ref } from "vue";
import { defineStore } from "pinia";
import type OrderServe from "@/types/OrderServe";
import type OrderList from "@/types/OrderList";
import { useCookStore } from "@/stores/orderlist";

const CookStore = useCookStore();

export const useServeStore = defineStore("serve", () => {
  const servefinish = ref<OrderServe[]>([]);
  const cookfinish = ref<OrderList[]>(CookStore.finish);

  const served = (id: number): void => {
    const index = cookfinish.value.findIndex((item) => item.id === id);
    servefinish.value.push(cookfinish.value[index]);
    cookfinish.value.splice(index, 1);
  };

  return {
    servefinish,
    cookfinish,
    served,
  };
});
