import { ref } from "vue";
import { defineStore } from "pinia";
import type Products from "@/types/Products";
import productService from "@/services/product";

export const useProductsStore = defineStore("Products", () => {
  const products = ref<Products[]>([]);
  // const products = ref<Products[]>([
  //   {
  //     order: 1,
  //     name: "ราเมง",
  //     total: 1,
  //     img: "picture/ราเมง.jpg",
  //     price: 50,
  //     category: "Food",
  //   },
  //   {
  //     order: 2,
  //     name: "ก๋วยเตี๋ยว",
  //     total: 1,
  //     img: "picture/เตี๋ยว.jpg",
  //     price: 70,
  //     category: "Food",
  //   },
  //   {
  //     order: 3,
  //     name: "มาม่า",
  //     total: 1,
  //     img: "picture/มาม่า.jpg",
  //     price: 80,
  //     category: "Food",
  //   },
  //   {
  //     order: 4,
  //     name: "กะเพาะปลา",
  //     total: 1,
  //     img: "picture/กะเพาะปลา.jpg",
  //     price: 90,
  //     category: "Food",
  //   },
  //   {
  //     order: 5,
  //     name: "อูด้ง",
  //     total: 1,
  //     img: "picture/อูด้ง.jpg",
  //     price: 40,
  //     category: "Food",
  //   },
  //   {
  //     order: 6,
  //     name: "โอวันติน",
  //     total: 1,
  //     img: "picture/โอวันติน.jpg",
  //     price: 20,
  //     category: "Drink",
  //   },
  //   {
  //     order: 7,
  //     name: "ชานมเย็น",
  //     total: 1,
  //     img: "picture/ชานมเย็น.jpg",
  //     price: 20,
  //     category: "Drink",
  //   },
  //   {
  //     order: 8,
  //     name: "กาแฟเย็น",
  //     total: 1,
  //     img: "picture/กาแฟเย็น.jpg",
  //     price: 20,
  //     category: "Drink",
  //   },
  //   {
  //     order: 9,
  //     name: "น้ำเปล่า",
  //     total: 1,
  //     img: "picture/น้ำเปล่า.jpg",
  //     price: 20,
  //     category: "Drink",
  //   },
  //   {
  //     order: 10,
  //     name: "เปปซี่",
  //     total: 1,
  //     img: "picture/เปปซี่.jpg",
  //     price: 20,
  //     category: "Drink",
  //   },
  //   {
  //     order: 11,
  //     name: "โค้ก",
  //     total: 1,
  //     img: "picture/โค้ก.jpg",
  //     price: 20,
  //     category: "Drink",
  //   },
  // ]);

  const getProducts = async () => {
    try {
      const res = await productService.getProducts();
      products.value = res.data;
      console.log(res.data);
    } catch (error) {
      console.log(error);
    }
    return;
  };

  return {
    products,
    getProducts,
  };
});
