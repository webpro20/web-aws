import { defineStore } from "pinia";
import type Table from "@/types/Table";
import { ref } from "vue";
import tableService from "@/services/table";
import orderService from "@/services/order";
import type Order from "@/types/Order";
import router from "@/router";
const Avaliable = ref(12);
const Used = ref(0);
const waited = ref(0);
const total = ref(0);
const currentTable = ref<Table>();
const currentOrder = ref<Order>();
const currentOrderId = ref<number>();
const dialog = ref(false);

export const useTableStore = defineStore("Table", () => {
  const currentStatus = ref<Table>({
    id: -1,
    name: "",
    amount: -1,
    status: "",
  });
  const tables = ref<Table[]>([]);
  const switchStatus = async (tables: Table) => {
    currentStatus.value = { ...tables };
    if (tables.status === "ใช้งาน") {
      dialog.value = false;
    } else {
      dialog.value = true;
    }
  };
  const saveStatus = async () => {
    const index = tables.value.findIndex(
      (item) => item.id === currentStatus.value.id
    );
    tables.value[index].status = currentStatus.value.status = "ใช้งาน";
    dialog.value = false;
    router.push("/point-of-sell");
    updateTable(index, tables.value[index]);
    await openTable(tables.value[index]);
  };

  async function getTables() {
    try {
      const res = await tableService.getTables();
      tables.value = res.data;
    } catch (error) {
      console.log(error);
    }
    return;
  }
  async function updateTable(id: number, table: Table) {
    try {
      const res = await tableService.updateTable(id, table);
    } catch (error) {
      console.log(error);
    }
    return;
  }

  async function openTable(table: Table) {
    console.log(currentTable.value);
    try {
      console.log("เปิดโต๊ะ: " + table.name);
      const newOrder: Order = {
        total: 0,
        amount: 0,
        status: "ยังไม่ชำระเงิน",
        orderItems: [],
        table: table,
      };
      const res = await orderService.saveOrder(newOrder);
      currentOrder.value = res.data;
      currentOrderId.value = res.data.id;
      console.log("ข้างล่างคือorderปัจจุบัน");
      console.log(currentOrder.value);
      // console.log(table);
    } catch (error) {
      console.log(error);
    }
    return;
  }

  return {
    tables,
    Avaliable,
    waited,
    Used,
    total,
    switchStatus,
    getTables,
    updateTable,
    currentTable,
    currentOrder,
    currentOrderId,
    dialog,
    currentStatus,
    saveStatus,
  };
});
