import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type Order from "@/types/Order";
import type Products from "@/types/Products";
import type OrderDetail from "@/types/OrderItem";
import orderServive from "@/services/order";
import type OrderItem from "@/types/OrderItem";
import orderService from "@/services/order";
import productQueue from "@/services/productQueue";

export const useOrderStore = defineStore("Order", () => {
  const orders = ref<Order[]>([]);
  const ordersTest = ref<Order[]>([]);

  const getAlloldVer = async (status = "") => {
    console.log(await productQueue.getProductQueueWithStatus("รอทำ"));
    const res = await orderService.getOrders();
    ordersTest.value = res.data;
    if (!status || status.length === 0) {
      return ordersTest;
    } else {
      const filter = ordersTest.value.filter(
        (e) => e.table?.status === status && e.status === "ยังไม่ชำระเงิน"
      );

      return filter;
    }
  };
  const getAll = async (status = "") => {
    const res = await productQueue.getProductQueueWithStatus(status);
    ordersTest.value = res.data;
    return ordersTest.value;
  };
  async function getOrders() {
    try {
      const res = await orderService.getOrders();
      console.log(res.data);
      ordersTest.value = res.data;
    } catch (error) {
      console.log(error);
    }
  }
  async function getOrdersWithStatus() {
    try {
    } catch (error) {
      console.log(error);
    }
  }
  const save = async (orderItem: OrderItem[]) => {
    const total = 0,
      amount = 0;
    // orderItem.forEach((value, index) => {
    //   total +=
    //     value.product.total! * (value.product.price! + value.choice_value);
    //   amount += value.product.total!;
    // });
    orders.value.push({
      amount: amount,
      total: total,
      orderItems: orderItem,
      // id_order: getCurrentIndex(),
      status: "waiting",
      table_id: 1,
    });
    const newOrder: Order = {
      amount: 0,
      total: 0,
      orderItems: orderItem,
      status: "waiting",
      table_id: 1,
    };

    await orderServive.saveOrder(orders);
  };

  const changeStatus = (id: number[], status: string) => {
    productQueue.changeStatus(id, status);
  };

  const getCurrentIndex = () => {
    return orders.value.length + 1;
  };

  const get = (find_index: number) => {
    const index = orders.value.findIndex((e) => e.id_order === find_index);
    return orders.value[index];
  };

  const convertStatusToText = (status: string) => {
    if (status === "waiting") return "รอ";
    else if (status === "cooking") return "ทำอาหาร";
    else if (status === "finish") return "เสร็จแล้ว";
    else return "ยกเลิก";
  };

  const convertLevelToText = (index?: number | string | undefined) => {
    const orderOptions = ref([
      { price: 0, label: "ธรรมดา", subtitle: "สามารถระบุเพิ่มเติมได้" },
      { price: 10, label: "พิเศษ", subtitle: "เพิ่ม 10 บาท" },
      { price: 20, label: "พิเศษแบบมากๆ", subtitle: "เพิ่ม 20 บาท" },
    ]);

    if (!index) return "ไม่มี";
    if (typeof index === "string") index = parseInt(index);
    return orderOptions.value[index - 1]?.label || "ไม่มี";
  };

  const getBillByTable = async (table_id: number) => {
    const res = await orderService.billByTable(table_id);
    return res.data;
  };

  const checkoutBill = async (order_id: number) => {
    const res = await orderService.checkoutBill(order_id);
    if (res.status === 200) return true;
    return false;
  };

  return {
    orders,
    getCurrentIndex,
    convertStatusToText,
    save,
    get,
    getAll,
    changeStatus,
    ordersTest,
    convertLevelToText,
    getOrders,
    checkoutBill,
    getBillByTable,
  };
});
